# TRAEFIK docker compose

[doc](https://hub.docker.com/_/traefik)

```bash
docker stack deploy -c docker-compose.yml traefik 
```

**get password hash**
```bash
echo $(htpasswd -nb user password) | sed -e s/\\$/\\$\\$/g
```

**add multiple instance of backend**
```bash
docker service scale <scale_name>=<number>
```